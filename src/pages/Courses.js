import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	//check to see if the mock data is captured
	console.log(coursesData);
	console.log(coursesData[0]);

	const courses = coursesData.map(course => {
		console.log(course);
		return(
			<CourseCard key={course.id} courseProp = {course}/>
		)
	})

	return (
		<>
		<h1> Available Courses: </h1>
		{courses}
		</>
	)
}

// process of passing 
//from the Courses Page => CourseCard = Php Laravel
//from the CourseCard -> props === parameter

//courseProp from the Courses Page === props parameter from CourseCard